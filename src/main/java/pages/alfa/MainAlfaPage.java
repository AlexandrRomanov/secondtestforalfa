package pages.alfa;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.Page;

public class MainAlfaPage extends Page {

    public MainAlfaPage(WebDriver browser) {
        super(browser);
    }

    public AlfaJobPage goToAlfaJob() {
        browser.findElement(By.xpath("//div/a[text()='Вакансии']")).click();
        return new AlfaJobPage(browser);
    }
}
