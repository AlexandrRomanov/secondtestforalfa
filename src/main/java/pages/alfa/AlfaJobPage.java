package pages.alfa;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.Page;

import java.util.stream.Collectors;

public class AlfaJobPage extends Page {

    public AlfaJobPage(WebDriver browser) {
        super(browser);
    }

    public AlfaJobPage goToAboutPage() {
        browser.findElement(By.xpath("//div[@id='menu']/a[text()='О нас']"))
                .click();
        return this;
    }

    public String getAboutInfo() {
        return String.join("\n"
                , browser.findElement(By.xpath("//h3[text()='О нас']/.."))
                .findElements(By.cssSelector("p"))
                .stream()
                .map(WebElement::getText)
                .collect(Collectors.toList()));
    }
}
