package pages.yandex;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.stream.Collectors;

public class MarketPage {

    private WebDriver browser;

    public MarketPage(WebDriver browser) {
        this.browser = browser;
    }

    public MarketPage chooseCategory(String category) {
        browser.findElement(By.xpath("//span[text()='" + category + "']"))
                .click();
        return this;
    }

    public MarketPage chooseSubcategory(String subcategory) {
        browser.findElement(By.xpath("//a[text()='" + subcategory + "']"))
                .click();
        return this;
    }

    public MarketPage setPriceFrom(Integer priceFrom) {
        browser.findElement(By.cssSelector("#glpricefrom"))
                .sendKeys(priceFrom.toString());
        waiter();
        return this;
    }

    public MarketPage setPriceTo(Integer priceTo) {
        browser.findElement(By.cssSelector("#glpriceto"))
                .sendKeys(priceTo.toString());
        waiter();
        return this;
    }

    public MarketPage setManufacturer(String manufacturer) {
        browser.findElement(By.xpath("//span[text()='" + manufacturer + "']"))
                .click();
        waiter();
        return this;
    }

    public List<String> getProductsNames() {
        List<String> productsNames = browser
                .findElements(
                By.cssSelector("div[data-id] div.n-snippet-cell2__title a"))
                .stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
        return productsNames;
    }

    public ProductPage goToProduct(String productName) {
        browser.findElement(By.cssSelector("a[title='" + productName + "']"))
                .click();
        return new ProductPage(browser);
    }

    private void waiter() {
        new WebDriverWait(browser, 10)
                .until(ExpectedConditions
                        .visibilityOfElementLocated(By.cssSelector("div.preloadable__preloader")));
        new WebDriverWait(browser, 10)
                .until(ExpectedConditions
                        .invisibilityOfElementLocated(By.cssSelector("div.preloadable__preloader")));
    }
}
