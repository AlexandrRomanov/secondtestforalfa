package pages.yandex;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ProductPage {

    private WebDriver browser;

    public ProductPage(WebDriver browser) {
        this.browser = browser;
    }

    public String getProductName() {
        return browser.findElement(By.cssSelector("h1.title")).getText();
    }
}
