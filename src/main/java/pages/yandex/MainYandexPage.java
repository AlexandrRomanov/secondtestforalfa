package pages.yandex;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MainYandexPage {

    private WebDriver browser;

    public MainYandexPage(WebDriver browser) {
        this.browser = browser;
        browser.get("https://yandex.ru/");
    }

    public SearchResultPage search(String whatToSearch) {
        browser.findElement(By.cssSelector("input#text"))
                .sendKeys(whatToSearch);
        browser.findElement(By.cssSelector("form[role='search'] button"))
                .click();
        return new SearchResultPage(browser);
    }

    public MarketPage goToMarket() {
        browser.findElement(By.cssSelector("a[data-id='market']"))
                .click();
        return new MarketPage(browser);
    }
}
