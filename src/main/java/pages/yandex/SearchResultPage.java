package pages.yandex;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SearchResultPage {

    private WebDriver browser;

    public SearchResultPage(WebDriver browser) {
        this.browser = browser;
    }

    public void goToResult(String name) {
        browser.findElement(By.xpath("//div[contains(concat(' ', @class, ' '),' path ')]//b[text()='" + name + "']"))
                .click();
        browser.switchTo().window(browser.getWindowHandles().toArray(new String[0])[1]);
    }
}
