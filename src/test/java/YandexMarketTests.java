import org.testng.annotations.*;
import org.testng.annotations.Test;
import pages.yandex.MainYandexPage;
import pages.yandex.MarketPage;

import static org.junit.Assert.assertEquals;

public class YandexMarketTests extends BaseTest {

    @Test
    public void smartphoneTest() {
        MarketPage marketPage = new MainYandexPage(browser)
                .goToMarket()
                .chooseCategory("Электроника")
                .chooseSubcategory("Мобильные телефоны")
                .setPriceFrom(40000);
        String firstProduct = marketPage.getProductsNames().get(0);
        String nameInProductPage = marketPage.goToProduct(firstProduct)
                .getProductName();
        assertEquals(firstProduct, nameInProductPage);
    }

    @Test
    public void headphonesTest() {
        MarketPage marketPage = new MainYandexPage(browser)
                .goToMarket()
                .chooseCategory("Электроника")
                .chooseSubcategory("Наушники и Bluetooth-гарнитуры")
                .setPriceFrom(17000)
                .setPriceTo(25000)
                .setManufacturer("Beats");
        String firstProduct = marketPage.getProductsNames().get(0);
        String nameInProductPage = marketPage.goToProduct(firstProduct)
                .getProductName();
        assertEquals(firstProduct, nameInProductPage);
    }

    @AfterTest(alwaysRun = true)
    public void cleanUp() {
        browser.quit();
    }
}
