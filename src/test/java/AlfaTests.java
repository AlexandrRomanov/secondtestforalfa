import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import pages.alfa.MainAlfaPage;
import pages.yandex.MainYandexPage;

import java.time.LocalDateTime;

public class AlfaTests extends BaseTest {

    @Test
    public void alfaJobAboutTest() {
        new MainYandexPage(browser)
                .search("AlfaBank.ru")
        .goToResult("AlfaBank.ru");
        String info = new MainAlfaPage(browser)
                .goToAlfaJob()
                .goToAboutPage()
                .getAboutInfo();
        saveInfo("Yandex", info);
    }

    @AfterTest(alwaysRun = true)
    public void cleanUp() {
        browser.quit();
    }
}
