import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.io.*;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class BaseTest {

    protected WebDriver browser;

    @BeforeTest
    protected void setUp() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        browser = new ChromeDriver();
    }

    protected void saveInfo(String searchEngin, String info) {
        String fileName = LocalDateTime.now().toString().replace('.', '_').replace(':', '_')
                        + ((RemoteWebDriver) browser).getCapabilities().getBrowserName()
                        + searchEngin
                        + ".txt";
        new File("result/").mkdirs();
        File file = new File("result/"+fileName);
        try {
            file.createNewFile();
            FileWriter writer = new FileWriter(file);
            writer.write(info);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
